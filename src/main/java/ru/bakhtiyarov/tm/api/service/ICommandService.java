package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import java.util.List;

public interface ICommandService {

   @NotNull
   List<AbstractCommand> getCommandList();

}
