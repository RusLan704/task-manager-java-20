package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void addAll(@Nullable List<E> tasks);

    void clearAll();

    @Nullable
    E findById(@NotNull String id);

    @Nullable
    E removeById(@Nullable String id);

}
