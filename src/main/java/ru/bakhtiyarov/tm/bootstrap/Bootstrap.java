package ru.bakhtiyarov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.*;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.system.UnknownArgumentException;
import ru.bakhtiyarov.tm.exception.system.UnknownCommandException;
import ru.bakhtiyarov.tm.repository.CommandRepository;
import ru.bakhtiyarov.tm.repository.ProjectRepository;
import ru.bakhtiyarov.tm.repository.TaskRepository;
import ru.bakhtiyarov.tm.repository.UserRepository;
import ru.bakhtiyarov.tm.service.*;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);


    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);


    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);


    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);


    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        registry(commandService.getCommandList());
    }

    private void registry(@NotNull final List<AbstractCommand> commands) {
        for (@NotNull final AbstractCommand command : commands) {
            registryCommands(command);
        }
    }

    private void registryCommands(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
         @Nullable  final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        try {
            argument.execute();
        } catch (Exception e) {
            logError(e);
        }
    }

    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authService.checkRole(command.roles());
        try {
            command.execute();
        } catch (Exception e) {
            logError(e);
        }
    }

    public void run(@Nullable final String... args) {
        System.out.println("Welcome to task manager!!!");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}