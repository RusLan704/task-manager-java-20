package ru.bakhtiyarov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user in program";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
