package ru.bakhtiyarov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataLoadException;
import sun.misc.BASE64Decoder;

import javax.imageio.IIOException;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 data from file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        @NotNull final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64date);
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
                @NotNull final ObjectInputStream objectOutputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            final Domain domain = (Domain) objectOutputStream.readObject();
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
