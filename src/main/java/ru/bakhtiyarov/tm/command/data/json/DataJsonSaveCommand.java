package ru.bakhtiyarov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataSaveException;

import javax.imageio.IIOException;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataJsonSaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from json file.";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        @NotNull final Domain domain = getDomain();

        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        ) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException(e);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
