package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull AbstractCommand command : commands) {
            @Nullable final String arg = command.arg();
            if (arg != null)
                System.out.println(command.arg());
        }
    }

}
