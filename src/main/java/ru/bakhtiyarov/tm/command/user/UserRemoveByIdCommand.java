package ru.bakhtiyarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user in program.";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE USER]");
        System.out.println("ENTER ID:");
        @NotNull  final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().removeById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
