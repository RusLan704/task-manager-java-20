package ru.bakhtiyarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserUpdateLastNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_LAST_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user last name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE LAST NAME]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        @Nullable final User userUpdated = serviceLocator.getUserService().updateUserLastName(userId, lastName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}