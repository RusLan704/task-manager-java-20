package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    @NotNull
    public EmptyRoleException() {
        super("Error! Role is empty...");
    }
    
}