package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    @NotNull
    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}
