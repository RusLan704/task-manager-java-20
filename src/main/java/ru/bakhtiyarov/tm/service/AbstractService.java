package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.api.service.IService;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public @NotNull List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(@Nullable List<E> entity) {
        if (entity == null) return;
        repository.addAll(entity);
    }

    @Override
    public void clearAll() {
        repository.clearAll();
    }

    @Nullable
    @Override
    public E findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public E removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

}
